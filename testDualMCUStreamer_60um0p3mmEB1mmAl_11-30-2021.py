# -*- coding: utf-8 -*-
"""
Created on Tue Oct  5 09:54:28 2021

@author: Verasonics
"""
import sys, serial, argparse
import serial.tools.list_ports
import numpy as np
from time import sleep
from collections import deque
import matplotlib
matplotlib.use("tkAgg")
import matplotlib.pyplot as plt 
import matplotlib.animation as animation
from scipy.signal import find_peaks
from threading import Thread
import threading
import time
import logging

logging.basicConfig(level=logging.DEBUG,
                    format='[%(levelname)s] (%(threadName)-10s) %(message)s',
                    )

fs=170e6;
epoxy_th=(635e-6-50e-6-285e-6);
epoxy_speed=2350;
al_th=0.6e-3;
al_speed=4500;
al_reflection=1;
time1=(epoxy_th*2)/epoxy_speed+(al_th*al_reflection*2)/al_speed;
time2=time1+200e-9;
class stm32Reader:
  # constr
      def __init__(self, target_pid, baudrate,avgs,acqLength,maxLen):   
          ports = serial.tools.list_ports.comports()
          self.st_port=[]
          for p in ports:
              if p.pid==target_pid:
                  #print(p.device)
                  self.st_port=str(p.device)
                  print("Found target ST mcu")
                  # open serial port
                  self.ser = serial.Serial(self.st_port, baudrate)
          self.allAcquisition = deque((np.array(np.zeros((acqLength,1))),np.array(np.zeros((acqLength,1)))))
          for i in range(avgs-2):
              self.allAcquisition.append(np.array(np.zeros((acqLength,1))+2))
          #print(str(np.size(self.allAcquisition[1])))
          
          self.avgAcquisition = np.average(np.array(self.allAcquisition),0) #need to create some pandas data buffer
          #print(str(self.avgAcquisition[-1][0]))
          self.avgs=avgs
          self.acqLength=acqLength
          self.ax= deque([0.0]*maxLen)
          self.maxLen=maxLen
      # add to buffer
      def addToBuf(self, buf, val):
          if len(buf) < self.maxLen:
              buf.append(val)
          else:
              buf.pop()
              buf.appendleft(val)
      
      # add data
      def add(self, data):
          #assert(len(data) == 1)#change to length of snapshot for snapshot
          self.addToBuf(self.ax, data)
          
          #self.addToBuf(self.ay, data[1])
      def run(self,fileName,acquisitions):
          global Au
          if self.st_port!=[]:
              line = 0 #start at 0 because our header is not real data
              searchForAcquisitionStart=True;
              acquisition=0
              while acquisition<acquisitions:
                  #print(str(acquisition))           
                  while line < self.acqLength:
                    if searchForAcquisitionStart==True:
                        tempData=str(self.ser.readline());
                        if tempData[3:6]=='ata':
                            searchForAcquisitionStart=False;
                            line=0;
                            acquisition+=1;
                            Data=np.array(np.zeros((self.acqLength,1)))
                    else:
                        line = line+1
                        getData=str(self.ser.readline())
                        #getData = ser.read()
                        #print(getData)
                        if getData[3:6]!='ata':
                            data=getData[2:][:-5]
        
                            #catch initial garbage samples and equate to 0
                            if data[2:4]!=str('00'):
                                Data[line-2]=data
                                #print('ST32 data:',str(data))
                                #file = open(fileName, "a")
                                #file.write(data+'\n') #write data with a newline
                            
                            
                            if line>=self.acqLength:
                                self.allAcquisition.popleft()
                                self.allAcquisition.append(Data)
                                self.avgAcquisition=np.median(np.array(self.allAcquisition),0)#average
                                
                                #original
                                #sample1=88#200 round(time1/(1/fs))
                                #sample2=122#594round(time2/(1/fs))
                                #print('ST rms :',str(np.sqrt(np.mean(self.avgAcquisition[sample1:sample2]**2))))
                                #self.add(np.sqrt(np.mean(self.avgAcquisition[sample1:sample2]**2)))
                                
                                #modified
                                sample00=41
                                sample0=75
                                sample1=105#92#200 round(time1/(1/fs))
                                sample2=120#594round(time2/(1/fs))
                                
                                #print('ST rms :',str(np.sqrt(np.mean(self.avgAcquisition[sample1:sample2]**2))))
                                
                                #self.add(np.sqrt(np.mean(self.avgAcquisition[sample1:sample2]**2)))   
                                print('ST rms :',str(np.sqrt(np.mean(self.avgAcquisition[sample1:sample2]**2))/np.sqrt(np.mean(self.avgAcquisition[sample00:sample0]**2))))

                                self.add(np.sqrt(np.mean(self.avgAcquisition[sample1:sample2]**2))/np.sqrt(np.mean(self.avgAcquisition[sample00:sample0]**2))) 
                                
                                #print('ST max normalized :',str(np.max(np.abs(self.avgAcquisition[sample1:sample2]))/np.max(np.abs(self.avgAcquisition[sample00:sample0]))))
                                #self.add(np.max(np.abs(self.avgAcquisition[sample1:sample2]))/np.max(np.abs(self.avgAcquisition[sample00:sample0]))) 
                                
                                #print('ST max :',str(np.max(np.abs(self.avgAcquisition[sample1:sample2]))))

                                #self.add(np.max(np.abs(self.avgAcquisition[sample1:sample2]))) 

                                Au=np.array(self.ax)
                                Au=list(Au);
                                #print('ST avg sample 599:',str(self.avgAcquisition[-2]))
                                searchForAcquisitionStart=True;
                                line=0;
          else:
                print("Did not find ST mcu")

class smartSurfaceReader:
  # constr
      def __init__(self, strPort, maxLen):    
          # open serial port
          self.ser = serial.Serial(strPort, 115200)
          self.ax = deque([0.0]*maxLen)
          #self.ay = deque([0.0]*maxLen)
          self.maxLen = maxLen

      
      # add to buffer
      def addToBuf(self, buf, val):
          if len(buf) < self.maxLen:
              buf.append(val)
          else:
              buf.pop()
              buf.appendleft(val)

      # add data
      def add(self, data):
          #assert(len(data) == 1)#change to length of snapshot for snapshot
          self.addToBuf(self.ax, data)
          #self.addToBuf(self.ay, data[1])
    
      # update plot
      def run(self,targetSamples):
          global Af
          totalSamples=0;
          while totalSamples<targetSamples:
              try:
                  #replace with snapshot acquisition code from dataGrabber
                  self.ser.write('0'.encode('utf-8'));
                  if (self.ser.inWaiting()>0):
                    myData = self.ser.readline()
                    data=int((myData.strip()).decode('utf-8'))
                  #add check on length for snapshot here  
                    self.add(data)
                    #processing such as row wise averaging is to be added here
                    print('Force data:',str(self.ax[0]))
                    #normalization
                    Af=np.array(self.ax)
                    #Af=(Af-min(Af))/(max(Af)-min(Af));
                    Af=list(Af);
                    totalSamples+=1;
                    time.sleep(0.5)
              except KeyboardInterrupt:
                  print('exiting')
          #print(Af[0])
          self.close() 
    
      # clean up
      def close(self):
          # close serial
          self.ser.flush()
          self.ser.close()  
          
def worker():
    logging.debug('Starting')
    time.sleep(2)
    logging.debug('Exiting')
    


def main():
   global Af,Au
   Af = range(0,100)
   Au = range(0,100)
   ULTRA_THRESH=0.96;
   FORCE_THRESH=0.9;
   FORCE_DELTA_THRESH=14;
   BASELINE_SAMPLES=40;
   BASELINE_PERIOD=True;
   # Create an object of class 
   strPort='COM4'
   
   Af_baseline=np.zeros((BASELINE_SAMPLES,1));
   Au_baseline=np.zeros((BASELINE_SAMPLES,1));
   
   mcuReader = smartSurfaceReader(strPort,100)
   
   targettedSamples=10000
   # Create a thread using member function of class 
   th = threading.Thread(target=mcuReader.run, args=(targettedSamples, ))
   
   # Create an object of ST class
   targetPID=14158
   baudrate=115200
   avgs=7
   filterLength=7
   acqLength=598
   stmReader = stm32Reader(targetPID,baudrate,avgs,acqLength,100)
   
   th2= threading.Thread(target=stmReader.run, args=('test.csv',40))
   #w = threading.Thread(name='worker', target=worker)
   # Start a thread
   #w.start()
   th.start()
   th2.start()
   # Print some logs in main thread
   '''time.sleep(2)
   for i in range(800):

       
       if i<BASELINE_SAMPLES:
            AF=-2*np.array(Af)/250.0;
            Af_baseline[i]=AF[0]
            Au_baseline[i]=np.array(Au)[0]
            #print(str(Af_baseline[i]))
       else:
            BASELINE_PERIOD=False
            ULTRA_THRESH=1.46
            FORCE_THRESH=FORCE_THRESH*np.median(Af_baseline)
       if BASELINE_PERIOD==False:     
           #print('Hi from Main Function')
           Af_now=-2*np.array(Af)/250.0;
           Au_now=np.array(Au);
           #plot outside baseline period
           plt.cla()
           #plt.scatter(np.linspace(1,len(Af),len(Af)),Af+2000*np.ones(np.size(Af)))
           plt.scatter(np.linspace(1,len(Af),len(Af)),Af_now)
           #print(str(max(2*np.array(Af)/200.0)))
           plt.scatter(np.linspace(1,len(Au),len(Au)),Au_now)
           plt.ylim([0,2.5])
           
           #extract the force values
           print('Force baseline:'+str(np.median(Af_baseline))+' U/S baseline:'+str(np.median(Au_baseline)))

           Af_now_val=Af_now[0];
           Au_now_val=Au_now[0];
           print('Force level:'+str(np.median(Af_now_val))+' U/S level:'+str(np.median(Au_now_val)))
           if Au_now_val<ULTRA_THRESH and Af_now_val<FORCE_THRESH:
                plt.title('Touch - Force + U/S triggered',fontsize=50)
                plt.pause(0.15)
           else:
                if Au_now_val>ULTRA_THRESH and Af_now_val>FORCE_THRESH:
                    plt.title('No touch-U/S veto triggered Force',fontsize=50)
                    plt.pause(0.15)
                else:
                    plt.title('No touch-Force + U/S not triggered',fontsize=50)
                    plt.pause(0.15)'''
   time.sleep(6)
   for i in range(800):

       if i<BASELINE_SAMPLES:
            AF=2*np.array(Af)/250.0;
            Af_baseline[i]=AF[0]
            AU=np.array(Au)
            Au_baseline[i]=AU[0]
            #print(str(Af_baseline[i]))
       else:
            if BASELINE_PERIOD==True:
                BASELINE_PERIOD=False
                Au_baseline=ULTRA_THRESH*np.median(Au_baseline)
                Af_baseline=2/250.0*(np.median(Af_baseline)*250/2)
       if BASELINE_PERIOD==False:     
           #print('Hi from Main Function')
           Af_now=2*np.array(Af)/250.0;
           Au_now_preFiltering=np.array(Au);
           
           Au_now=np.zeros((len(Au_now_preFiltering-filterLength),1));
           for i in range(len(Au_now)):
                Au_now[i]=np.mean(Au_now_preFiltering[i:i+filterLength-1]);
           #plot outside baseline period
           plt.cla()
           #plt.scatter(np.linspace(1,len(Af),len(Af)),Af+2000*np.ones(np.size(Af)))
           plt.scatter(np.linspace(1,len(Af)-avgs,len(Af)-avgs),Af_now[avgs-1:-1])
           #print(str(max(2*np.array(Af)/200.0)))
           plt.scatter(np.linspace(1,len(Au),len(Au)),Au_now)
           plt.ylim([-0.5,2.5])
           
           #extract the force values
           print('Force baseline:'+str(np.median(Af_baseline)*250/2.0)+' U/S baseline:'+str(np.median(Au_baseline)))

           Af_now_val=np.median(Af_now[0:avgs-1]);
           #Au_now_val=Au_now[0];
           Au_now_val=np.median(Au_now[0:avgs-1]);
           print('Force level:'+str(np.median(Af_now_val)*250/2.0)+' U/S level:'+str(np.median(Au_now_val)))
           if Au_now_val>Au_baseline and np.abs(250/2.0*(Af_now_val-Af_baseline))>FORCE_DELTA_THRESH:
                #plt.title('Press - Force + U/S triggered',fontsize=50)
                plt.pause(0.15)
           else:
                #plt.title('No press-Force + U/S not triggered',fontsize=50)
                plt.pause(0.15)
                #if Au_now_val>=Au_baseline and np.abs(-250/2.0*(Af_now_val-Af_baseline))<=FORCE_DELTA_THRESH:
                #    plt.title('No touch-U/S veto triggered Force',fontsize=50)
                #    plt.pause(0.15)
                #else:
                #    plt.title('No touch-Force + U/S not triggered',fontsize=50)
                #    plt.pause(0.15)'''


   # Wait for thread to exit
   th.join()
   
if __name__ == '__main__':
   main()